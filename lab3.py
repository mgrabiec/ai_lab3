import numpy as np
from scipy import linalg, sparse
import pandas as pd

from sklearn import neighbors, datasets, preprocessing
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
iris = datasets.load_iris()
X, y = iris.data[:, :2], iris.target
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=33)
scaler = preprocessing.StandardScaler().fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)
knn = neighbors.KNeighborsClassifier(n_neighbors=5)
knn.fit(X_train, y_train)
y_pred = knn.predict(X_test)
accuracy_score(y_test, y_pred)

from sklearn.linear_model import LinearRegression
lr = LinearRegression(normalize=True)
from sklearn.svm import SVC
svc = SVC(kernel='linear')
from sklearn.naive_bayes import GaussianNB
gnb = GaussianNB()
from sklearn.decomposition import PCA
pca = PCA(n_components=0.95)
from sklearn.cluster import KMeans
k_means = KMeans(n_clusters=3, random_state=0)

import matplotlib.pyplot as plt
from matplotlib.cbook import get_sample_data

import seaborn as sns

from bokeh.plotting import figure
from bokeh.io import output_file, show

# NUMPY
a = np.array([1,2,3])

b = np.array([(1.5,2,3), (4,5,6)], dtype = float)

c = np.array([[(1.5,2,3), (4,5,6)], [(3,2,1), (4,5,6)]], dtype = float)

print('Array dimensions\n')
print(a.shape)

print('Length of array\n')
print(len(a))

print('Number of array dimensions\n')
print(b.ndim)

print('Number of array elements\n')
print(b.dtype)

print('Data type of array elements\n')
print(b.astype(int))

# SciPy linear algebra

A = np.matrix(np.random.random((2,2)))
B = np.asmatrix(b)
C = np.mat(np.random.random((10,5)))
D = np.mat([[3,4], [5,6]])

print('Create a 2X2 identity matrix\n')
F = np.eye(3, k=1) 
print(F)
print('Create a 2x2 identity matrix\n')
G = np.mat(np.identity(2)) 
print(G)
print('Compressed Sparse Row matrix\n')
C[C > 0.5] = 0
H = sparse.csr_matrix(C)
print(H)
print('Compressed Sparse Column matrix\n')
I = sparse.csc_matrix(D) 
print(I)
print('Dictionary Of Keys matrix\n')
J = sparse.dok_matrix(A)
print(J)
print('Identify sparse matrix\n')
print(sparse.isspmatrix_csc(A)) 

# Pandas Basic
s = pd.Series([3, -5, 7, 4], index=['a', 'b', 'c', 'd'])

data = {'Country': ['Belgium', 'India', 'Brazil'],
        'Capital': ['Brussels', 'New Delhi', 'Brasília'],
        'Population': [11190846, 1303171035, 207847528]}
df = pd.DataFrame(data,columns=['Country', 'Capital', 'Population'])

print('Sort by labels along an axis\n')
print(df.sort_index())
print('Sort by the values along an axis\n')
print(df.sort_values(by='Country'))
print('Assign ranks to entries\n')
print(df.rank())
print('Number of non-NA values\n')
print(df.count())
print('Describe DataFrame columns\n')
print(df.columns )

# Sckit-Learn

print('Accuracy Score\n')
knn.score(X_test, y_test)
from sklearn.metrics import accuracy_score
print(accuracy_score(y_test, y_pred))

print('Classification Report\n')
from sklearn.metrics import classification_report
print(classification_report(y_test, y_pred))

print('Confusion Matrix\n')
from sklearn.metrics import confusion_matrix
print(confusion_matrix(y_test, y_pred))

print('Mean Absolute Error\n')
y_pred=y_pred[:3]
from sklearn.metrics import mean_absolute_error
y_true = [3, -0.5, 2]
print(mean_absolute_error(y_true, y_pred))

print('R² Score\n')
from sklearn.metrics import r2_score
print(r2_score(y_true, y_pred))


# Matplotlib

x = np.linspace(0, 10, 100)
y = np.cos(x)
z = np.sin(x)

fig, ax = plt.subplots()
img = np.load(get_sample_data('axes_grid/bivariate_normal.npy',asfileobj=False))
im = ax.imshow(img,cmap='gist_earth',interpolation='nearest',vmin=-2,vmax=2)

# colors colorbars & color maps
plt.plot(x, x, x, x**2, x, x**3)
ax.plot(x, y, alpha = 0.4)
ax.plot(x, y, c='k')
fig.colorbar(im, orientation='horizontal')
im = ax.imshow(img, cmap='seismic')

plt.show()

# Markers
fig, ax = plt.subplots()
ax.scatter(x,y,marker=".")
ax.plot(x,y,marker="o")

plt.show()

# Linestyles
lines = ax.plot(x,y) 



plt.plot(x,y,linewidth=4.0)
plt.plot(x,y,ls='solid')
plt.plot(x,y,ls='--')
plt.plot(x,y,'--',x**2,y**2,'-.')
plt.setp(lines,color='r',linewidth=4.0)


# Save plot
plt.savefig('foo.png')

# Mathtext
plt.title(r'$sigma_i=15$', fontsize=20)
plt.show()

# Seaborn
import pandas as pd
import numpy as np
uniform_data = np.random.rand(10, 12)
data = pd.DataFrame({'x':np.arange(1,101), 'y':np.random.normal(0,4,100)})

titanic = sns.load_dataset("titanic")
iris = sns.load_dataset("iris")

sns.set_theme(style="darkgrid")
tips = sns.load_dataset("tips")
sns.relplot(x="total_bill", y="tip", data=tips)
plt.show()

# Bar Chart
sns.barplot(x="sex", y="survived",hue="class", data=titanic)
plt.show()

# Count Plot
sns.countplot(x="deck", data=titanic, palette="Greens_d")
plt.show()

# Point Plot
sns.pointplot(x="class",y="survived",hue="sex",data=titanic,palette={"male":"g","female":"m"},markers=["^","o"], linestyles=["-","--"])
plt.show()

# Boxplot
sns.boxplot(x="alive",y="age",hue="adult_male", data=titanic)
sns.boxplot(data=iris,orient="h")
plt.show()

#  Violinplot
sns.violinplot(x="age", y="sex", hue="survived", data=titanic)
plt.show()

# Bokeh

x = [1, 2, 3, 4, 5]
y = [6, 7, 2, 4, 5]
p = figure(title="simple line example",x_axis_label='x',_axis_label='y')
p.line(x, y, legend_label="Temp.", line_width=2)
output_file("lines.html")
show(p)